package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity {

    public Project(@NotNull String userId, @NotNull String name) {
        super(userId, name);
    }

    public Project(@NotNull String userId, @NotNull String name, @Nullable String description) {
        super(userId, name, description);
    }

}
