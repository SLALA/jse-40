package ru.t1.strelcov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.Session;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.CryptUtil;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AuthServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private static final String LOGIN_UNIQUE = "Login_Unique";

    @NotNull
    private static final String PASS = "PASS";

    @NotNull
    private final String PASS_HASH = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);

    @NotNull
    final List<User> users = Arrays.asList(
            new User("test1", PASS_HASH, Role.USER),
            new User("test2", PASS_HASH, Role.ADMIN));

    @Before
    public void before() {
        for (@NotNull final User user : users) {
            try {
                @NotNull User existedUser = userService.findByLogin(user.getLogin());
                userService.removeByLogin(existedUser.getLogin());
                userService.removeByLogin(LOGIN_UNIQUE);
            } catch (@NotNull Exception ignored) {
            }
        }
        try {
            userService.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
        userService.addAll(users);
    }

    @After
    public void after() {
        try {
            for (@NotNull final User user : users)
                userService.removeById(user.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            userService.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
    }

    @SneakyThrows
    @Test
    public void loginTest() {
        Assert.assertThrows(AbstractException.class, () -> authService.login(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> authService.login("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> authService.login(users.get(0).getLogin(), "INCORRECT_PASS"));
        @NotNull final User user = users.get(0);
        @NotNull final String token = authService.login(user.getLogin(), PASS);
        @NotNull final String sessionKey = propertyService.getSessionSecret();
        @NotNull final String json = CryptUtil.decrypt(token, sessionKey);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        Assert.assertEquals(user.getId(), session.getUserId());
        Assert.assertEquals(user.getRole(), session.getRole());
        Assert.assertNotNull(session.getDate());
        userService.lockUserByLogin(users.get(0).getLogin());
        Assert.assertThrows(AbstractException.class, () -> authService.login(users.get(0).getLogin(), PASS));
    }

    @Test
    public void logoutTest() {
    }

    @SneakyThrows
    @Test
    public void validateTokenTest() {
        Assert.assertThrows(AbstractException.class, () -> authService.validateToken("notExistedId"));
        @NotNull final User user = users.get(0);
        @NotNull final String token = authService.login(user.getLogin(), PASS);
        @NotNull final Session session = authService.validateToken(token);
        Assert.assertEquals(user.getId(), session.getUserId());
        Assert.assertEquals(user.getRole(), session.getRole());
        Assert.assertNotNull(session.getDate());
        @NotNull final String sessionKey = propertyService.getSessionSecret();
        @NotNull String json = CryptUtil.decrypt(token, sessionKey);
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session sessionFromToken = objectMapper.readValue(json, Session.class);
        Assert.assertEquals(sessionFromToken, session);
        Assert.assertNotNull(sessionFromToken.getDate());
        sessionFromToken.setDate(new Date(sessionFromToken.getDate().getTime() - propertyService.getSessionTimeout() * 1000 * 60 - 10000 * 60));
        objectMapper = new ObjectMapper();
        json = objectMapper.writeValueAsString(sessionFromToken);
        @NotNull final String changedDateToken = CryptUtil.encrypt(json, sessionKey);
        Assert.assertThrows(AbstractException.class, () -> authService.validateToken(changedDateToken));
    }

}
