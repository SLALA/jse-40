package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.Session;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable String token);

    @NotNull
    Session validateToken(@Nullable String token);

}
