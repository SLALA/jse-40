package ru.t1.strelcov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll();


    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByUserId(@Param("userId") @NotNull final String userId);

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE user_id = #{userId} ORDER BY ${sortColumnName}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByUserIdSorted(@Param("userId") @NotNull final String userId, @Param("sortColumnName") @NotNull final String sortColumnName);

    @Insert("INSERT INTO tm_task(id, user_id, name, description, status, created, start_date, project_id)" +
            " VALUES(#{id}, #{userId}, #{name}, #{description}, #{status}, #{created}, #{dateStart}, #{projectId})")
    void add(@NotNull final Task task);

    @Update("UPDATE tm_task" +
            " SET user_id = #{userId}, name = #{name}, description = #{description}, status = #{status}, created = #{created}, start_date = #{dateStart}, project_id = #{projectId}" +
            " WHERE id = #{id}")
    void update(@NotNull final Task task);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findById(@Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findByIdByUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE user_id = #{userId} AND name = #{name} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findByName(@Param("userId") @NotNull final String userId, @Param("name") @NotNull final String name);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull final String userId);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void remove(@NotNull final Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date, project_id FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

}
