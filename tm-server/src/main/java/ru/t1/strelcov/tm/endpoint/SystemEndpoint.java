package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;

import javax.jws.WebService;
import java.util.Optional;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint")
@NoArgsConstructor
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        response.setEmail(serviceLocator.getPropertyService().getEmail());
        response.setName(serviceLocator.getPropertyService().getName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        response.setVersion(serviceLocator.getPropertyService().getVersion());
        return response;
    }

}
