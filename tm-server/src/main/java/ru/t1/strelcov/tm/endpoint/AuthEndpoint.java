package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.model.Session;
import ru.t1.strelcov.tm.model.User;

import javax.jws.WebService;
import java.util.Optional;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint")
@NoArgsConstructor
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new UserLoginResponse(serviceLocator.getAuthService().login(request.getLogin(), request.getPassword()));
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse getProfile(@NotNull final UserProfileRequest request) {
        @NotNull final Session session = check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        @NotNull final User user = serviceLocator.getUserService().findById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
