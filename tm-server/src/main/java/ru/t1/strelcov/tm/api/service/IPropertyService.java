package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    String getName();

    @NotNull
    String getVersion();

    @NotNull
    String getEmail();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSecret();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getBackupStatus();

    @NotNull
    String getDataBaseUrl();

    @NotNull
    String getDataBaseUserLogin();

    @NotNull
    String getDataBaseUserPassword();

    @NotNull
    String getDataBaseDriver();

}
