package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project removeProjectWithTasksById(@Nullable String userId, @Nullable String projectId);

}
