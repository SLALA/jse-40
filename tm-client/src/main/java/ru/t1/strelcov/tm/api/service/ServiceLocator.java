package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.*;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IDataEndpoint getDataEndpoint();

    @NotNull
    ITokenService getTokenService();

}
